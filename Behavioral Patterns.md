## Chain of Responsibility Pattern
- Here is how sending a request works in the application using the Chain of Responsibility: the Client in need of a request to be handled sends it to the chain of handlers, which are classes that extend the Handler class. 
- Each of the handlers in the chain takes its turn at trying to handle the request it receives from the client.
- If ConcreteHandler_i can handle it, then the request is handled, if not it is sent to the handler ConcreteHandler_i+1, the next one in the chain.
## Command Pattern
- The waiter (Invoker) takes the order from the customer on his pad. The order is then queued for the order cook and gets to the cook (Receiver) where it is processed.
- In this case the actors in the scenario are the following: The Client is the customer. He sends his request to the receiver through the waiter, who is the Invoker.
- The waiter encapsulates the command (the order in this case) by writing it on the check and then places it, creating the ConcreteCommand object which is the command itself. 
- The Receiver will be the cook that, after completing work on all the orders that were sent to him before the command in question, starts work on it. 
- Another noticeable aspect of the example is the fact that the pad for the orders does not support only orders from the menu, so it can support commands to cook many different items.
## Interpreter Pattern
- The Interpreter pattern is used exaustively in defining grammars, tokenize input and store it.
- A specific area where Interpreter can be used are the rules engines.
- The Interpreter pattern can be used to add functionality to the composite pattern.
- Can be used on: Roman Numerals Convertor
## Iterator Pattern
- access contents of a collection without exposing its internal structure.
- support multiple simultaneous traversals of a collection.
- provide a uniform interface for traversing different collection.
```ruby
class MyClass
  def initialize(data)
    @data = data
  end

  # Define an iterator method
  def iterate
    @data.each do |element|
      yield(element)
    end
  end
end
```
## Mediator Pattern
- Define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently.
- Example: when a new value is selected from a ComboBox object a Label has to display a new value. Both the ComboBox and the Label are not aware of each other structure and all the interaction is managed by the Dialog object.
```ruby
# Mediator
class ChatRoom
  def initialize
    @users = []
  end

  def register(user)
    @users << user
    user.chat_room = self
  end

  def send_message(sender, recipient, message)
    recipient.receive_message(sender, message)
  end
end

# Colleague
class User
  attr_accessor :name, :chat_room

  def initialize(name)
    @name = name
  end

  def send_message(recipient, message)
    @chat_room.send_message(self, recipient, message)
  end

  def receive_message(sender, message)
    puts "#{name} received a message from #{sender.name}: #{message}"
  end
end
```
## Memento Pattern
- The intent of this pattern is to capture the internal state of an object without violating encapsulation and thus providing a mean for restoring the object into initial state when needed.
- Used to maintain history of a object to use undo, redo
## Observer Pattern
- Defines a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.
## Strategy Pattern
- There are common situations when classes differ only in their behavior.
- For this cases is a good idea to isolate the algorithms in separate classes in order to have the ability to select different algorithms at runtime.
- This allows the system to easily switch between different algorithms on runtime
## Template Method Pattern
- Define the skeleton of an algorithm in an operation, deferring some steps to subclasses.
- Template Method lets subclasses redefine certain steps of an algorithm without letting them to change the algorithm's structure.
## Visitor Pattern
- The Visitor Pattern is a behavioral design pattern that allows you to separate algorithms or operations from the objects on which they operate. 
- It's particularly useful when you have a complex structure of objects and you want to perform different operations on these objects without modifying their classes.

```ruby
# Element interface
class Employee
end

# Concrete element classes
class Manager < Employee
  attr_reader :name, :salary

  def initialize(name, salary)
    @name = name
    @salary = salary
  end

  def accept(visitor)
    visitor.visit_manager(self)
  end
end

class Engineer < Employee
  attr_reader :name, :salary

  def initialize(name, salary)
    @name = name
    @salary = salary
  end

  def accept(visitor)
    visitor.visit_engineer(self)
  end
end

# Visitor interface
class BonusCalculator
end

# Concrete visitor classes
class StandardBonusCalculator < BonusCalculator
  def visit_manager(manager)
    bonus = manager.salary * 0.1 # 10% bonus for managers
    puts "#{manager.name} received a standard bonus of $#{bonus}"
  end

  def visit_engineer(engineer)
    bonus = engineer.salary * 0.05 # 5% bonus for engineers
    puts "#{engineer.name} received a standard bonus of $#{bonus}"
  end
end

class SeniorBonusCalculator < BonusCalculator
  def visit_manager(manager)
    bonus = manager.salary * 0.15 # 15% bonus for senior managers
    puts "#{manager.name} received a senior bonus of $#{bonus}"
  end

  def visit_engineer(engineer)
    bonus = engineer.salary * 0.07 # 7% bonus for senior engineers
    puts "#{engineer.name} received a senior bonus of $#{bonus}"
  end
end
```
## Null Object Pattern
- Provide an object as a surrogate for the lack of an object of a given type.
- The Null Object Pattern provides intelligent do nothing behavior, hiding the details from its collaborators.
- example: 