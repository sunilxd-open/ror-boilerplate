[source](https://www.freecodecamp.org/news/the-basic-design-patterns-all-developers-need-to-know/)
All design pattern can be classified into this 3 category
- **Creational**: These patterns are designed for class instantiation. They can be either class-creation patterns or object-creational patterns.
- **Structural**: These patterns are designed with regard to a class's structure and composition. The main goal of most of these patterns is to increase the functionality of the class(es) involved, without changing much of its composition.
- **Behavioral**: These patterns are designed depending on how one class communicates with others.

## Singleton Design Pattern (Creational)
- Objective is to create only one instance of a class and to provide only one global access point to that object.

## Decorator Design Pattern (Structural)
- The decorator design pattern falls into the structural category, that deals with the actual structure of a class
- The goal of this design is to modify an objects’ functionality at runtime.

## Command Design Pattern (Behavioral)
- The main focus of the command pattern is to inculcate a higher degree of loose coupling between involved parties
- Coupling is the way that two (or more) classes that interact with each other, well, interact. The ideal scenario when these classes interact is that they do not depend heavily on each other. That’s loose coupling. So, a better definition for loose coupling would be, classes that are interconnected, making the least use of each other.
- The need for this pattern arose when requests needed to be sent without consciously knowing what you are asking for or who the receiver is.