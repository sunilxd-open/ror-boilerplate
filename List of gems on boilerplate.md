Here is the list of all the gems mentioned in your `Gemfile`:

1. [x] `rails`, '~> 6.0.4', '>= 6.0.4.6'
2. [x] `pg`, '>= 0.18', '< 2.0'
3. [x] `puma`, '~> 4.1'
4. [ ] ~~`sass-rails`, '>= 6'~~ only api why sass?
5. [x] `redis`, '~> 4.0'
6. [x] `bcrypt`, '~> 3.1.7'
7. [x] `image_processing`, '~> 1.2'
8. [x] `bootsnap`, '>= 1.4.2', require: false
9. [x] `activerecord-session_store`
10. [x] `kaminari`
11. [x] `api-pagination`
12. [x] `faker`, git: 'https://github.com/faker-ruby/faker.git', branch: 'main'
13. [x] `factory_bot_rails`
14. [x] `active_model_serializers`, '~> 0.10.0'
15. [x] `active_service`, github: 'chloerei/active_service'
16. [x] `dotenv-rails`
17. [ ] ~~`font-awesome-rails`~~ not needed on api only service
18. [ ] ~~`simple_form`~~
19. [ ] ~~`client_side_validations`~~
20. [ ] ~~`client_side_validations-simple_form`~~
21. [ ] ~~`jquery-ui-rails`~~
22. [ ] ~~`rails_sortable`~~

### Gems in Development and Test Group:
23. [ ] ~~`byebug`, platforms: [:mri, :mingw, :x64_mingw]~~ rials/debug offers`byebug`
24. [x] `rspec-rails`
25. [x] `rspec_api_documentation`
26. [ ] ~~`reqres_rspec`~~ don't know it's purpose but using old aws sdk (not maintained)
27. [ ] ~~`stripe-ruby-mock`, '~> 3.0.1', require: 'stripe_mock'~~ only need if need to test stripe integration

### Gems in Development Group:
28. [x] `web-console`, '>= 3.3.0'
29. [ ] ~~`listen`, '>= 3.0.5', '< 3.2'~~
30. [x] `spring`
31. [x] `spring-watcher-listen`
32. [ ] ~~`capistrano`~~ needed only if deploy using capistrano automation
33. [ ] ~~`capistrano-rails`~~
34. [ ] ~~`capistrano3-puma`~~
35. [ ] ~~`capistrano-rvm`~~
36. [ ] ~~`capistrano-nvm`, require: false~~

### Gems in Test Group:
37. [x] `shoulda-matchers`, '~> 3.1'
38. [x] `database_cleaner`
39. [x] `binding_of_caller`
40. [x] `simplecov`, require: false

### Windows-specific Gem:
41. [x] `tzinfo-data`, platforms: [:mingw, :mswin, :x64_mingw, :jruby]