## Adapter Pattern
- Convert the interface of a class into another interface clients expect.
- Adapter lets classes work together, that could not otherwise because of incompatible interfaces.
## Bridge Pattern
- The intent of this pattern is to decouple abstraction from implementation so that the two can vary independently.
- Example: object that handles persistence of objects over different platforms using either relational databases or file system structures (files and folders). segregation of persistence from DB
## Composite Pattern
- The intent of this pattern is to compose objects into tree structures to represent part-whole hierarchies.
- Composite lets clients treat individual objects and compositions of objects uniformly.
## Decorator Pattern
- The decorator design pattern is a structural pattern in object-oriented programming that lets you add new functionality to an existing object without altering its original code. 
- It's like adding features to an object by wrapping it in a special "accessory" class.
## Flyweight Pattern
- The intent of this pattern is to use sharing to support a large number of objects that have part of their internal state in common where the other part of state can vary
## Proxy Pattern
- The intent of this pattern is to provide a "Placeholder" for an object to control references to it.
- Sometimes we need the ability to control the access to an object. For example if we need to use only a few methods of some costly objects we'll initialize those objects when we need them entirely. 
- Until that point we can use some light objects exposing the same interface as the heavy objects