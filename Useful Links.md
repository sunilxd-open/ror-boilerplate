-  https://dev.to/eclecticcoding/rails-boilerplate-4hgf
	- About rails boilerplate
- https://jumpstartrails.com/
	- Paid boilerplate for SAAS integrations
	- and open source free template https://github.com/excid3/jumpstart/blob/master/template.rb
- https://github.com/mattbrictson/nextgen?tab=readme-ov-file
	- simple tool which can be used instead of rails new 
- https://github.com/rootstrap/rails_api_base
	- rails api boilerplate with some features
- https://alexkitchens.net/rails-source-code
	- About rails internals
- https://github.com/emad-elsaid/rubrowser
	- Old project to visualise the ruby code
- https://github.com/seattlerb/flay
	- Gem to identify same code "repetition"