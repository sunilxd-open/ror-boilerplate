## Singleton Pattern
- Ensure that only one instance of a class is created and Provide a global access point to the object.
- Used when we want only one instance of an object
## Factory Pattern
- Newly created object through a common interface.
- Used when we want common class to create any class
- rails `factory_bot` is a great example
## Factory Method Pattern
- creates objects without exposing the instantiation logic to the client.
- Used when we want the type of object which need to be created depended on subclasses
```ruby
class Main
	def factory_method(*args)
		return new Main(*args)
	end
end

class SubMain < Main
	def initialize(name)
	    @name = name
	end
end
```
## Abstract Factory Pattern
- Abstract Factory offers the interface for creating a **family of related objects**, without explicitly specifying their classes.
- Abstract Factory is a super-factory which creates other factories (Factory of factories)
```ruby
class TruckFactory
	def open_cargo
		return new OpenCargo
	end

	def closed_cargo
		return new ClosedCargo
	end
end

class CarFactory
	def sedan
		return new Sedan
	end

	def suv
		return new SUV
	end
end

class FactoryMaker
	def initialize(type)
		return (type == "car") ? new CarFactory : new TruckFactory
	end
end
```
## Builder Pattern
- The Builder design pattern uses the Factory Builder pattern to decide which concrete class to initiate in order to build the **desired type** of object
- Example: `factory_bot` traits, which is used to create particular type of objects
## Prototype Pattern
- specifying the kind of objects to create using a prototypical instance
- creating new objects by copying this prototype
- It is more convenient to copy an existing instance than to create a new one.
```ruby
obj1 = new SomeClass;
obj2 = obj1.clone();
```
## Object Pool Pattern
- reuse and share objects that are expensive to create.
- ReusablePool - manage the reusable objects for use by Clients, creating and managing a pool of objects.
- When a client asks for a Reusable object, the pool performs the following actions:
	- Search for an available Reusable object and if it was found it will be returned to the client.  
	- If no Reusable object was found then it tries to create a new one. If this actions succeds the new Reusable object will be returned to the client.  
	- If the pool was unable to create a new Reusable, the pool will wait until a reusable object will be released.
- we'll use an object pool whenever there are several clients who needs the same stateless resource which is expensive to create.