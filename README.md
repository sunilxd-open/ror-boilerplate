## About the files

[big_project_info](big_project_info.csv) contains basic info of 9 open source projects found on [this page](https://github.com/asyraffff/Open-Source-Ruby-and-Rails-Apps)

## Design Pattern
[Creational Patterns](Creational%20Patterns.md)  
[Behavioral Patterns](Behavioral%20Patterns.md)  
[Structural Patterns](Structural%20Patterns.md)